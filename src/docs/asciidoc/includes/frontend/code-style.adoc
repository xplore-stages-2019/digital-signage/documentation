=== Code style

==== TS lint

____
TSLint is an extensible static analysis tool that checks TypeScript code for readability, maintainability, and functionality errors.
____

We used this to ensure the same code style in the project (e.g. single qoutes instead of double quotes).
Intellij also highlights TS lint errors and suggests a fix, which is very helpful.

<<<

==== Codacy

____
https://www.codacy.com[Codacy] automates code reviews and monitors code quality over time. Static analysis, code coverage and metrics for Ruby, JavaScript, PHP, Scala, Java, Python, CoffeeScript and CSS.
____

.Codacy overview
image::./codacy/codacy.png[Codacy overview]

Codacy gives a grade based on the quality of the project. Grades range from A to F, A being the highest grade.
Our project received an *A* grade.

Our project also has 0 issues.
Codacy looks for a variety of different issues, like:

* Security: security issues, potential vulnerabilities, unsafe dependencies.
* Error Prone: bad practices/patterns that cause code to fail/prone to bugs.
* Code Style: related to the style of the code, line length, tabs vs spaces.
* Compatibility: identifies code that has problems with older systems or cross platform support.
* Unused Code: unnecessary code not being used.
* Performance: inefficiently written code.

Codacy also reports we have a 72% test coverage.

.Codacy issue breakdown and coverage
image::./codacy/codacy-details.png[Codacy issue breakdown and coverage]

.See the results for yourself
****
https://app.codacy.com/project/xplorestages2019/digital-signage-display/dashboard[https://app.codacy.com/project/xplorestages2019/digital-signage-display/dashboard]
****
