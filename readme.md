# Digital signage documentation

Documentation for digital-signage-display and conference-service.

## Usage

Run `./gradlew asciidoctor` to build the project.<br />
Navigate to `build/asciidoc/html5` or `build/asciidoc/pdf` for the HTML or PDF version respectively.
